# Dockerfile

FROM openjdk:15-jdk-alpine
MAINTAINER artemjonas
RUN mkdir -p /target
COPY target/backend-0.0.1-SNAPSHOT.jar /target/backend-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/target/backend-0.0.1-SNAPSHOT.jar"]
