package com.example.backend.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity
@EnableAutoConfiguration
@Table(name = "rooms")
public class RoomDao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String name;

    private UUID storey_id;

    public RoomDao(String name, UUID storey_id) {
        this.name = name;
        this.storey_id = storey_id;
    }

    public RoomDto toDto() {
        return new RoomDto(name);
    }

}
