package com.example.backend.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity
@EnableAutoConfiguration
@Table(name = "buildings")
public class BuildingDao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String name;
    private String address;

    public BuildingDao(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public BuildingDto toDto() {
        return new BuildingDto(name, address);
    }

}
