package com.example.backend.service;

import com.example.backend.entity.BuildingDao;

import java.util.List;
import java.util.UUID;

public interface BuildingService {

    List<BuildingDao> getAllBuildings();

    BuildingDao getBuildingById(UUID uuid);

    BuildingDao addNewBuilding(String name, String address);

    BuildingDao updateBuilding(UUID uuid, String name, String address);

    boolean deleteBuilding(UUID uuid) throws Exception;

}
