package com.example.backend.service;

import com.example.backend.entity.BuildingDao;
import com.example.backend.entity.StoreyDao;
import com.example.backend.repository.BuildingRepository;
import com.example.backend.repository.StoreyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
public class BuildingServiceImpl implements BuildingService{

    @Autowired
    BuildingRepository buildingRepository;
    @Autowired
    StoreyRepository storeyRepository;

    @Override
    public List<BuildingDao> getAllBuildings() {
        List<BuildingDao> list = buildingRepository.findAll();
        if (!list.isEmpty()) {
            return list;
        }
        throw new NoSuchElementException();
    }

    @Override
    public BuildingDao getBuildingById(UUID uuid) {
        return buildingRepository.findById(uuid).orElseThrow();
    }

    @Override
    public BuildingDao addNewBuilding(String name, String address) {
        return buildingRepository.save(new BuildingDao(name, address));
    }

    @Override
    public BuildingDao updateBuilding(UUID uuid, String name, String address) {
        BuildingDao building = buildingRepository.findById(uuid).orElseThrow();
        building.setName(name);
        building.setAddress(address);
        return buildingRepository.save(building);
    }

    @Override
    public boolean deleteBuilding(UUID uuid) {
        for (StoreyDao s : storeyRepository.findAll()) {
            if (s.getBuilding_id().equals(uuid)) {
                throw new IllegalCallerException();
            }
        }
        getBuildingById(uuid);
        buildingRepository.deleteById(uuid);
        return true;
    }
}
