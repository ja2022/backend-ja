package com.example.backend.service;

import com.example.backend.entity.RoomDao;
import com.example.backend.repository.RoomRepository;
import com.example.backend.repository.StoreyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
public class RoomServiceImpl implements RoomService{

    @Autowired
    RoomRepository roomRepository;
    @Autowired
    StoreyRepository storeyRepository;

    @Override
    public List<RoomDao> getAllRooms() {
        List<RoomDao> list = roomRepository.findAll();
        if (!list.isEmpty()) {
            return list;
        }
        throw new NoSuchElementException();
    }

    @Override
    public RoomDao getRoomById(UUID uuid) {
        return roomRepository.findById(uuid).orElseThrow();
    }

    @Override
    public RoomDao addNewRoom(String name, UUID storey_id) {
        storeyRepository.findById(storey_id).orElseThrow();
        return roomRepository.save(new RoomDao(name, storey_id));
    }

    @Override
    public RoomDao updateRoom(UUID uuid, String name, UUID storey_id) {
        storeyRepository.findById(storey_id).orElseThrow();
        RoomDao room = roomRepository.findById(uuid).orElseThrow();
        room.setName(name);
        room.setStorey_id(storey_id);
        return roomRepository.save(room);
    }

    @Override
    public boolean deleteRoom(UUID uuid) {
        getRoomById(uuid);
        roomRepository.deleteById(uuid);
        return true;
    }
}
