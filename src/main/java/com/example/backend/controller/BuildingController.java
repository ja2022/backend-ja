package com.example.backend.controller;

import com.example.backend.entity.BuildingDao;
import com.example.backend.request.AddBuildingRequest;
import com.example.backend.request.UpdateBuildingRequest;
import com.example.backend.service.BuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/assets/buildings/")
public class BuildingController {

    @Autowired
    BuildingService buildingService;

    @GetMapping()
    public ResponseEntity<List<BuildingDao>> getAllBuildings() {
        try {
            System.out.println("Try getting all buildings...");
            return new ResponseEntity<>(buildingService.getAllBuildings(), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.out.println("No buildings found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{Id}")
    public ResponseEntity<BuildingDao> getBuildingById(@PathVariable UUID Id) {
        try {
            System.out.println("Try getting building...");
            return new ResponseEntity<>(buildingService.getBuildingById(Id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.out.println("Building not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    public ResponseEntity<BuildingDao> addNewBuilding(@RequestBody AddBuildingRequest addBuildingRequest) {
        try {
            System.out.println("Try adding new building...");
            return new ResponseEntity<>(buildingService.addNewBuilding(
                    addBuildingRequest.getName(),
                    addBuildingRequest.getAddress()),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{Id}")
    public ResponseEntity<BuildingDao> updateBuilding(@PathVariable UUID Id, @RequestBody UpdateBuildingRequest updateBuildingRequest) {
        UUID bodyId = updateBuildingRequest.getId();
        if (bodyId != null && !bodyId.equals(Id)) {
            System.out.println("BuildingId in URL and Body not equal");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        try {
            System.out.println("Try updating building...");
            return new ResponseEntity<>(buildingService.updateBuilding(
                    Id,
                    updateBuildingRequest.getName(),
                    updateBuildingRequest.getAddress()),
                    HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.out.println("Building not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{Id}")
    public ResponseEntity<Boolean> deleteBuilding(@PathVariable UUID Id) {
        try {
            System.out.println("Try deleting building...");
            return new ResponseEntity<>(buildingService.deleteBuilding(Id), HttpStatus.OK);
        } catch (IllegalCallerException e) {
            System.out.println("Not deleted. There are storeys in that building");
            return new ResponseEntity<>(HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        } catch (NoSuchElementException e) {
            System.out.println("Building not found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}