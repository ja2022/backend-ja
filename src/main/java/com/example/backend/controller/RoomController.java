package com.example.backend.controller;

import com.example.backend.entity.RoomDao;
import com.example.backend.request.AddRoomRequest;
import com.example.backend.request.UpdateRoomRequest;
import com.example.backend.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/assets/rooms/")
public class RoomController {

    @Autowired
    RoomService roomService;

    @GetMapping()
    public ResponseEntity<List<RoomDao>> getAllRooms() {
        try {
            System.out.println("Try getting all rooms...");
            return new ResponseEntity<>(roomService.getAllRooms(), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.out.println("No rooms found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{Id}")
    public ResponseEntity<RoomDao> getRoomById(@PathVariable UUID Id) {
        try {
            System.out.println("Try getting room...");
            return new ResponseEntity<>(roomService.getRoomById(Id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.out.println("Building not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    public ResponseEntity<RoomDao> addNewRoom(@RequestBody AddRoomRequest addRoomRequest) {
        try {
            System.out.println("Try adding new room...");
            return new ResponseEntity<>(roomService.addNewRoom(
                    addRoomRequest.getName(),
                    addRoomRequest.getStorey_id()),
                    HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.out.println("Storey not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{Id}")
    public ResponseEntity<RoomDao> updateRoom(@PathVariable UUID Id, @RequestBody UpdateRoomRequest updateRoomRequest) {
        UUID bodyId = updateRoomRequest.getId();
        if (bodyId != null && !bodyId.equals(Id)) {
            System.out.println("RoomId in URL and Body not equal");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        try {
            System.out.println("Try updating room...");
            return new ResponseEntity<>(roomService.updateRoom(
                    Id,
                    updateRoomRequest.getName(),
                    updateRoomRequest.getStorey_id()),
                    HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.out.println("Room or storey not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{Id}")
    public ResponseEntity<Boolean> deleteRoom(@PathVariable UUID Id) {
        try {
            System.out.println("Try deleting room...");
            return new ResponseEntity<>(roomService.deleteRoom(Id), HttpStatus.OK);
        } catch (IllegalCallerException e) {
            System.out.println("Not deleted. There are reservations for rooms in that building");
            return new ResponseEntity<>(HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        } catch (NoSuchElementException e) {
            System.out.println("Room not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}